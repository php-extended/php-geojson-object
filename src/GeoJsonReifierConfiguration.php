<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use InvalidArgumentException;
use PhpExtended\Reifier\ReifierConfiguration;

/**
 * GeoJsonReifierConfiguration class file.
 * 
 * Configuration specific to the geo json library.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class GeoJsonReifierConfiguration extends ReifierConfiguration
{
	
	/**
	 * Constructor.
	 * @throws InvalidArgumentException
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->addIgnoreExcessFields(GeoJsonPoint::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonLineString::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonPolygon::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonMultiPoint::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonMultiLineString::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonMultiPolygon::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonGeometryCollection::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonFeature::class, ['type']);
		$this->addIgnoreExcessFields(GeoJsonFeatureCollection::class, ['type']);
		
		$this->setImplementation(GeoJsonBoundingBoxInterface::class, GeoJsonBoundingBox::class);
		$this->setImplementation(GeoJsonPointCoordinateInterface::class, GeoJsonPointCoordinate::class);
		$this->setImplementation(GeoJsonPointInterface::class, GeoJsonPoint::class);
		$this->setImplementation(GeoJsonLineStringCoordinateInterface::class, GeoJsonLineStringCoordinate::class);
		$this->setImplementation(GeoJsonLineStringInterface::class, GeoJsonLineString::class);
		$this->setImplementation(GeoJsonPolygonCoordinateInterface::class, GeoJsonPolygonCoordinate::class);
		$this->setImplementation(GeoJsonPolygonInterface::class, GeoJsonPolygon::class);
		$this->setImplementation(GeoJsonMultiPointCoordinateInterface::class, GeoJsonMultiPointCoordinate::class);
		$this->setImplementation(GeoJsonMultiPointInterface::class, GeoJsonMultiPoint::class);
		$this->setImplementation(GeoJsonMultiLineStringCoordinateInterface::class, GeoJsonMultiLineStringCoordinate::class);
		$this->setImplementation(GeoJsonMultiLineStringInterface::class, GeoJsonMultiLineString::class);
		$this->setImplementation(GeoJsonMultiPolygonCoordinateInterface::class, GeoJsonMultiPolygonCoordinate::class);
		$this->setImplementation(GeoJsonMultiPolygonInterface::class, GeoJsonMultiPolygon::class);
		$this->setImplementation(GeoJsonGeometryCollectionInterface::class, GeoJsonGeometryCollection::class);
		$this->setImplementation(GeoJsonFeatureInterface::class, GeoJsonFeature::class);
		$this->setImplementation(GeoJsonFeatureCollectionInterface::class, GeoJsonFeatureCollection::class);
		
		$this->setIterableInnerTypes(GeoJsonLineStringCoordinate::class, ['points'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonLineString::class, ['coordinates'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonPolygon::class, ['coordinates'], 'array');
		$this->setIterableInnerTypes(GeoJsonPolygon::class, ['coordinates[]'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonMultiPoint::class, ['coordinates'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonMultiLineString::class, ['coordinates'], 'array');
		$this->setIterableInnerTypes(GeoJsonMultiLineString::class, ['coordinates[]'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonMultiPolygon::class, ['coordinates'], 'array');
		$this->setIterableInnerTypes(GeoJsonMultiPolygon::class, ['coordinates[]'], 'array');
		$this->setIterableInnerTypes(GeoJsonMultiPolygon::class, ['coordinates[][]'], GeoJsonPointCoordinate::class);
		$this->setIterableInnerTypes(GeoJsonFeature::class, ['properties'], 'array');
		$this->setIterableInnerTypes(GeoJsonFeature::class, ['properties[]'], 'string');
		
		$this->setIterableInnerTypes(GeoJsonGeometryCollection::class, ['geometries'], GeoJsonGeometryInterface::class);
		$this->setIterableInnerTypes(GeoJsonFeatureCollection::class, ['features'], GeoJsonFeatureInterface::class);
		
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'Point', GeoJsonPoint::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'LineString', GeoJsonLineString::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'MultiPoint', GeoJsonMultiPoint::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'Polygon', GeoJsonPolygon::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'MultiLineString', GeoJsonMultiLineString::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'MultiPolygon', GeoJsonMultiPolygon::class);
		$this->addPolymorphism(GeoJsonGeometryInterface::class, 0, 'type', 'GeometryCollection', GeoJsonGeometryCollection::class);
		
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'Point', GeoJsonPoint::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'LineString', GeoJsonLineString::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'MultiPoint', GeoJsonMultiPoint::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'Polygon', GeoJsonPolygon::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'MultiLineString', GeoJsonMultiLineString::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'MultiPolygon', GeoJsonMultiPolygon::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'GeometryCollection', GeoJsonGeometryCollection::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'Feature', GeoJsonFeature::class);
		$this->addPolymorphism(GeoJsonObjectInterface::class, 0, 'type', 'FeatureCollection', GeoJsonFeatureCollection::class);
	}
	
}
