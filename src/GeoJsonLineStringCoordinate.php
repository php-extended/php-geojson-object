<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;
use RuntimeException;

/**
 * GeoJsonLineStringCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonLineStringCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonLineStringCoordinate implements GeoJsonLineStringCoordinateInterface
{
	
	/**
	 * The points.
	 * 
	 * @var array<integer, GeoJsonPointCoordinateInterface>
	 */
	protected array $_points = [];
	
	/**
	 * Builds a new GeoJsonLineStringCoordinate with its dependancies.
	 * 
	 * @param array<integer, GeoJsonPointCoordinateInterface> $points
	 */
	public function __construct(array $points)
	{
		$this->_points = $points;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_points as $point)
		{
			$arrstr[] = $point->__toString();
		}
		
		return '['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonLineStringCoordinateInterface::getFirstPoint()
	 * @throws RuntimeException
	 */
	public function getFirstPoint() : GeoJsonPointCoordinateInterface
	{
		foreach($this->_points as $point)
		{
			return $point;
		}
		
		throw new RuntimeException('The string contains no points.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonLineStringCoordinateInterface::getLastPoint()
	 * @throws RuntimeException
	 */
	public function getLastPoint() : GeoJsonPointCoordinateInterface
	{
		$point = null;
		
		foreach($this->_points as $p)
		{
			$point = $p;
		}
		
		if(null !== $point)
		{
			return $point;
		}
		
		throw new RuntimeException('The string contains no points.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonLineStringCoordinateInterface::getPoints()
	 */
	public function getPoints() : Iterator
	{
		return new ArrayIterator($this->_points);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitLineStringCoordinate($this);
	}
	
}
