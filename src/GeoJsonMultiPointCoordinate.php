<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;

/**
 * GeoJsonMultiPointCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonMultiPointCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonMultiPointCoordinate implements GeoJsonMultiPointCoordinateInterface
{
	
	/**
	 * The points.
	 * 
	 * @var array<integer, GeoJsonPointCoordinateInterface>
	 */
	protected $_points;
	
	/**
	 * Builds a new GeoJsonMultiPointCoordinate with its dependancies.
	 * 
	 * @param array<integer, GeoJsonPointCoordinateInterface> $points
	 */
	public function __construct(array $points)
	{
		$this->_points = $points;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_points as $point)
		{
			$arrstr[] = $point->__toString();
		}
		
		return '['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonMultiPointCoordinateInterface::getPoints()
	 */
	public function getPoints() : Iterator
	{
		return new ArrayIterator($this->_points);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonCoordinateInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitMultiPointCoordinate($this);
	}
	
}
