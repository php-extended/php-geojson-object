<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;
use RuntimeException;

/**
 * GeoJsonPolygonCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonPolygonCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonPolygonCoordinate implements GeoJsonPolygonCoordinateInterface
{
	
	/**
	 * The lines of the polygon.
	 * 
	 * @var array<integer, GeoJsonLineStringCoordinateInterface>
	 */
	protected array $_lines = [];
	
	/**
	 * Builds a new GeoJsonPolyconCoordinate with the given data.
	 * 
	 * @param array<int, array<int, GeoJsonPointCoordinateInterface>> $coordinates
	 */
	public function __construct(array $coordinates)
	{
		foreach($coordinates as $points)
		{
			$this->_lines[] = new GeoJsonLineStringCoordinate($points);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$coords = [];
		
		foreach($this->_lines as $line)
		{
			$coords[] = $line->__toString();
		}
		
		return '['.\implode(', ', $coords).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPolygonCoordinateInterface::getExteriorLine()
	 * @throws RuntimeException
	 */
	public function getExteriorLine() : GeoJsonLineStringCoordinateInterface
	{
		foreach($this->_lines as $line)
		{
			return $line;
		}
		
		throw new RuntimeException('The string contains no points.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPolygonCoordinateInterface::getInteriorLines()
	 */
	public function getInteriorLines() : Iterator
	{
		$lines = [];
		
		foreach($this->_lines as $k => $line)
		{
			if(0 < $k)
			{
				$lines[] = $line;
			}
		}
		
		return new ArrayIterator($lines);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPolygonCoordinateInterface::getLines()
	 */
	public function getLines() : Iterator
	{
		return new ArrayIterator($this->_lines);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonCoordinateInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitPolygonCoordinate($this);
	}
	
}
