<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonObject class file.
 * 
 * This class is a simple implementation of the GeoJsonObjectInterface.
 * 
 * @author Anastaszor
 */
abstract class GeoJsonObject implements GeoJsonObjectInterface
{
	
	/**
	 * The bounding box of this object.
	 * 
	 * @var ?GeoJsonBoundingBoxInterface
	 */
	protected ?GeoJsonBoundingBoxInterface $_boundingBox;
	
	/**
	 * Builds a new GeoJsonObject with the given bbox.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox)
	{
		$this->_boundingBox = $bbox;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getBoundingBox()
	 */
	public function getBoundingBox() : ?GeoJsonBoundingBoxInterface
	{
		return $this->_boundingBox;
	}
	
}
