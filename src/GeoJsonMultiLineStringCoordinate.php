<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;

/**
 * GeoJsonMultiLineStringCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonMultiLineStringCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonMultiLineStringCoordinate implements GeoJsonMultiLineStringCoordinateInterface
{
	
	/**
	 * The line strings.
	 * 
	 * @var array<integer, GeoJsonLineStringCoordinate>
	 */
	protected array $_lineStrings = [];
	
	/**
	 * Builds a new GeoJsonMultiLineStringCoordinate with its dependancies.
	 * 
	 * @param array<int, array<int, GeoJsonPointCoordinateInterface>> $lineStrings
	 */
	public function __construct(array $lineStrings)
	{
		foreach($lineStrings as $points)
		{
			$this->_lineStrings[] = new GeoJsonLineStringCoordinate($points);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_lineStrings as $lineStrings)
		{
			$arrstr[] = $lineStrings->__toString();
		}
		
		return '['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinateInterface::getLineStrings()
	 */
	public function getLineStrings() : Iterator
	{
		return new ArrayIterator($this->_lineStrings);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonCoordinateInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitMultiLineStringCoordinate($this);
	}
	
}
