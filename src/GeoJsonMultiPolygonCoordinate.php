<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;

/**
 * GeoJsonMultiPolygonCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonMultiPolygonCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonMultiPolygonCoordinate implements GeoJsonMultiPolygonCoordinateInterface
{
	
	/**
	 * The polygons.
	 * 
	 * @var array<integer, GeoJsonPolygonCoordinateInterface>
	 */
	protected array $_polygons = [];
	
	/**
	 * Builds a new GeoJsonPolygonCoordinate with its dependancies.
	 * 
	 * @param array<integer, array<integer, array<integer, GeoJsonPointCoordinateInterface>>> $polygons
	 */
	public function __construct(array $polygons)
	{
		foreach($polygons as $coordinates)
		{
			$this->_polygons[] = new GeoJsonPolygonCoordinate($coordinates);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_polygons as $polygon)
		{
			$arrstr[] = $polygon->__toString();
		}
		
		return '['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinateInterface::getPolygons()
	 */
	public function getPolygons() : Iterator
	{
		return new ArrayIterator($this->_polygons);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonCoordinateInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitMultiPolygonCoordinate($this);
	}
	
}
