<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;

/**
 * GeoJsonGeometryCollection class file.
 * 
 * This class is a simple implementation of the GeoJsonGeometryCollectionInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonGeometryCollection extends GeoJsonObject implements GeoJsonGeometryCollectionInterface
{
	
	/**
	 * The geometries.
	 * 
	 * @var array<integer, GeoJsonGeometryInterface>
	 */
	protected array $_geometries = [];
	
	/**
	 * Builds a new GeoJsonGeometryCollection with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param array<integer, GeoJsonGeometryInterface> $geometries
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, array $geometries)
	{
		$this->_geometries = $geometries;
		parent::__construct($bbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_geometries as $geometry)
		{
			$arrstr[] = $geometry->__toString();
		}
		
		return 'COLLECTION ['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'GeometryCollection';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonGeometryCollectionInterface::getGeometries()
	 */
	public function getGeometries() : Iterator
	{
		return new ArrayIterator($this->_geometries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $this->beVisitedByGeometry($visitor);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonGeometryInterface::beVisitedByGeometry()
	 */
	public function beVisitedByGeometry(GeoJsonGeometryVisitorInterface $visitor)
	{
		return $visitor->visitGeometryCollection($this);
	}
	
}
