<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPoint class file.
 * 
 * This class is a simple implementation of the GeoJsonPointInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonPoint extends GeoJsonObject implements GeoJsonPointInterface
{
	
	/**
	 * The coordinates.
	 * 
	 * @var GeoJsonPointCoordinateInterface
	 */
	protected GeoJsonPointCoordinateInterface $_coordinates;
	
	/**
	 * Builds a new GeoJsonPoint with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param GeoJsonPointCoordinateInterface $coordinates
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, GeoJsonPointCoordinateInterface $coordinates)
	{
		$this->_coordinates = $coordinates;
		parent::__construct($bbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'POINT '.$this->_coordinates->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'Point';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPointInterface::getCoordinates()
	 */
	public function getCoordinates() : GeoJsonPointCoordinateInterface
	{
		return $this->_coordinates;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $this->beVisitedByGeometry($visitor);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonGeometryInterface::beVisitedByGeometry()
	 */
	public function beVisitedByGeometry(GeoJsonGeometryVisitorInterface $visitor)
	{
		return $visitor->visitPoint($this);
	}
	
}
