<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonMultiPoint class file.
 * 
 * This class is a simple implementation of the GeoJsonMultiPointInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonMultiPoint extends GeoJsonObject implements GeoJsonMultiPointInterface
{
	
	/**
	 * The coordinates.
	 * 
	 * @var GeoJsonMultiPointCoordinateInterface
	 */
	protected GeoJsonMultiPointCoordinateInterface $_coordinates;
	
	/**
	 * Builds a new GeoJsonMultiPoint with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param array<int, GeoJsonPointCoordinateInterface> $coordinates
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, array $coordinates)
	{
		$this->_coordinates = new GeoJsonMultiPointCoordinate($coordinates);
		parent::__construct($bbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'MULTI POINT '.$this->_coordinates->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'MultiPoint';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonMultiPointInterface::getCoordinates()
	 */
	public function getCoordinates() : GeoJsonMultiPointCoordinateInterface
	{
		return $this->_coordinates;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $this->beVisitedByGeometry($visitor);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonGeometryInterface::beVisitedByGeometry()
	 */
	public function beVisitedByGeometry(GeoJsonGeometryVisitorInterface $visitor)
	{
		return $visitor->visitMultiPoint($this);
	}
	
	
}
