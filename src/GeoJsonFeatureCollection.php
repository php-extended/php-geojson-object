<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use ArrayIterator;
use Iterator;

/**
 * GeoJsonFeatureCollection class file.
 * 
 * This object is a simple implementation of the GeoJsonFeatureCollectionInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonFeatureCollection extends GeoJsonObject implements GeoJsonFeatureCollectionInterface
{
	
	/**
	 * The features.
	 * 
	 * @var array<integer, GeoJsonFeatureInterface>
	 */
	protected $_features;
	
	/**
	 * Builds a new GeoJsonFeatureCollection with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param array<integer, GeoJsonFeatureInterface> $features
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, array $features)
	{
		parent::__construct($bbox);
		$this->_features = $features;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$arrstr = [];
		
		foreach($this->_features as $feature)
		{
			$arrstr[] = $feature->__toString();
		}
		
		return 'FEATURE COLLECTION ['.\implode(', ', $arrstr).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'FeatureCollection';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonFeatureCollectionInterface::getFeatures()
	 */
	public function getFeatures() : Iterator
	{
		return new ArrayIterator($this->_features);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $visitor->visitFeatureCollection($this);
	}
	
}
