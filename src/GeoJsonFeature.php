<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonFeature class file.
 * 
 * This class is a simple implementation of the GeoJsonFeatureInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonFeature extends GeoJsonObject implements GeoJsonFeatureInterface
{
	
	/**
	 * The identifier of the feature, if exists.
	 * 
	 * @var ?string
	 */
	protected ?string $_identifier;
	
	/**
	 * The geometry of the feature, if localized.
	 * 
	 * @var GeoJsonGeometryInterface
	 */
	protected GeoJsonGeometryInterface $_geometry;
	
	/**
	 * The properties object.
	 * 
	 * @var array<integer|string, string>
	 */
	protected array $_properties = [];
	
	/**
	 * Builds a new GeoJsonFeature with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param ?string $identifier
	 * @param GeoJsonGeometryInterface $geometry
	 * @param array<integer|string, string> $properties
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, ?string $identifier, GeoJsonGeometryInterface $geometry, array $properties = [])
	{
		parent::__construct($bbox);
		$this->_identifier = $identifier;
		$this->_geometry = $geometry;
		$this->_properties = $properties;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'FEATURE '.$this->_geometry->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'Feature';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonFeatureInterface::getIdentifier()
	 */
	public function getIdentifier() : ?string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonFeatureInterface::getGeometry()
	 */
	public function getGeometry() : GeoJsonGeometryInterface
	{
		return $this->_geometry;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonFeatureInterface::getProperties()
	 * @psalm-suppress InvalidReturnType
	 */
	public function getProperties() : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->_properties;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $visitor->visitFeature($this);
	}
	
}
