<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonBoundingBox class file.
 * 
 * This class is a simple implementation of the GeoJsonBoundingBoxInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonBoundingBox implements GeoJsonBoundingBoxInterface
{
	
	/**
	 * The west plane longitude of the box.
	 * 
	 * @var float
	 */
	protected float $_west = 0.0;
	
	/**
	 * The north plane latitude of the box.
	 * 
	 * @var float
	 */
	protected float $_north = 0.0;
	
	/**
	 * The min height of the box.
	 * 
	 * @var ?float
	 */
	protected ?float $_depth = null;
	
	/**
	 * The east plane longitude of the box.
	 * 
	 * @var float
	 */
	protected float $_east = 0.0;
	
	/**
	 * The south plane latitude of the box.
	 * 
	 * @var float
	 */
	protected float $_south = 0.0;
	
	/**
	 * The max height of the box.
	 * 
	 * @var ?float
	 */
	protected ?float $_height = null;
	
	/**
	 * Builds a new GeoJsonBoundingBox with its dependancies.
	 * 
	 * When only 4 parameters are given, those are : west, north, east, south.
	 * 
	 * @param float $west
	 * @param float $north
	 * @param float $depth
	 * @param float $east
	 * @param ?float $south
	 * @param ?float $height
	 */
	public function __construct(float $west, float $north, float $depth, float $east, ?float $south = null, ?float $height = null)
	{
		$this->_west = $west;
		$this->_north = $north;
		
		if(null === $south)
		{
			$this->_east = $depth;
			$this->_south = $east;
			
			return;
		}
		
		$this->_depth = $depth;
		$this->_east = $east;
		$this->_south = $south;
		$this->_height = $height;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$str = '['.((string) $this->_west).', '.((string) $this->_north)
			.(null !== $this->_depth ? ', '.((string) $this->_depth) : '');
		
		$str .= ', '.((string) $this->_east).', '.((string) $this->_south)
			.(null !== $this->_height ? ', '.((string) $this->_height) : '');
		
		return $str.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getWest()
	 */
	public function getWest() : float
	{
		return $this->_west;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getNorth()
	 */
	public function getNorth() : float
	{
		return $this->_north;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getDepth()
	 */
	public function getDepth() : ?float
	{
		return $this->_depth;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getEast()
	 */
	public function getEast() : float
	{
		return $this->_east;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getSouth()
	 */
	public function getSouth() : float
	{
		return $this->_south;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonBoundingBoxInterface::getHeight()
	 */
	public function getHeight() : ?float
	{
		return $this->_height;
	}
	
}
