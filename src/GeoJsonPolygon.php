<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPolygon class file.
 * 
 * This class is a simple implementation of the GeoJsonPolygonInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonPolygon extends GeoJsonObject implements GeoJsonPolygonInterface
{
	
	/**
	 * The coordinates.
	 * 
	 * @var GeoJsonPolygonCoordinateInterface
	 */
	protected GeoJsonPolygonCoordinateInterface $_coordinates;
	
	/**
	 * Builds a new GeoJsonPolygon with its dependancies.
	 * 
	 * @param ?GeoJsonBoundingBoxInterface $bbox
	 * @param array<int, array<int, GeoJsonPointCoordinateInterface>> $coordinates
	 */
	public function __construct(?GeoJsonBoundingBoxInterface $bbox, array $coordinates)
	{
		$this->_coordinates = new GeoJsonPolygonCoordinate($coordinates);
		parent::__construct($bbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'POLYGON '.$this->_coordinates->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::getType()
	 */
	public function getType() : string
	{
		return 'Polygon';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPolygonInterface::getCoordinates()
	 */
	public function getCoordinates() : GeoJsonPolygonCoordinateInterface
	{
		return $this->_coordinates;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonObjectInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor)
	{
		return $this->beVisitedByGeometry($visitor);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonGeometryInterface::beVisitedByGeometry()
	 */
	public function beVisitedByGeometry(GeoJsonGeometryVisitorInterface $visitor)
	{
		return $visitor->visitPolygon($this);
	}
	
}
