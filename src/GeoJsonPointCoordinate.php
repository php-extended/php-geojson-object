<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPointCoordinate class file.
 * 
 * This class is a simple implementation of the GeoJsonPointCoordinateInterface.
 * 
 * @author Anastaszor
 */
class GeoJsonPointCoordinate implements GeoJsonPointCoordinateInterface
{
	
	/**
	 * The latitude of the point.
	 * 
	 * @var float
	 */
	protected float $_latitude = 0.0;
	
	/**
	 * The longitude of the point.
	 * 
	 * @var float
	 */
	protected float $_longitude = 0.0;
	
	/**
	 * The altitude of the point.
	 * 
	 * @var ?float
	 */
	protected ?float $_altitude = null;
	
	/**
	 * Builds a new GeoJsonPointCoordinate with its dependancies.
	 * 
	 * @param float $longitude
	 * @param float $latitude
	 * @param ?float $altitude
	 */
	public function __construct(float $longitude, float $latitude, ?float $altitude = null)
	{
		$this->_longitude = $longitude;
		$this->_latitude = $latitude;
		$this->_altitude = $altitude;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '['.((string) $this->_longitude).', '.((string) $this->_latitude)
			.(null !== $this->_altitude ? ', '.((string) $this->_altitude) : '').']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPointCoordinateInterface::getLatitude()
	 */
	public function getLatitude() : float
	{
		return $this->_latitude;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPointCoordinateInterface::getLongitude()
	 */
	public function getLongitude() : float
	{
		return $this->_longitude;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonPointCoordinateInterface::getAltitude()
	 */
	public function getAltitude() : ?float
	{
		return $this->_altitude;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\GeoJson\GeoJsonCoordinateInterface::beVisitedBy()
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor)
	{
		return $visitor->visitPointCoordinate($this);
	}
	
}
