<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonLineStringCoordinateTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonLineStringCoordinate
 *
 * @internal
 *
 * @small
 */
class GeoJsonLineStringCoordinateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonLineStringCoordinate
	 */
	protected GeoJsonLineStringCoordinate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[[1, 2], [3, 4]]', $this->_object->__toString());
	}
	
	public function testGetFirstPoint() : void
	{
		$this->assertEquals(new GeoJsonPointCoordinate(1.0, 2.0), $this->_object->getFirstPoint());
	}
	
	public function testGetFirstPointFailed() : void
	{
		$this->expectException(RuntimeException::class);
		
		(new GeoJsonLineStringCoordinate([]))->getFirstPoint();
	}
	
	public function testGetLastPoint() : void
	{
		$this->assertEquals(new GeoJsonPointCoordinate(3.0, 4.0), $this->_object->getLastPoint());
	}
	
	public function testGetLastPointFailed() : void
	{
		$this->expectException(RuntimeException::class);
		
		(new GeoJsonLineStringCoordinate([]))->getLastPoint();
	}
	
	public function testGetPoints() : void
	{
		$this->assertEquals(new ArrayIterator([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]), $this->_object->getPoints());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonLineStringCoordinate([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
	}
	
}
