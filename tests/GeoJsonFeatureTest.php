<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonFeature;
use PhpExtended\GeoJson\GeoJsonObject;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonFeatureTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonFeature
 *
 * @internal
 *
 * @small
 */
class GeoJsonFeatureTest extends TestCase
{
	
	/**
	 * The feature.
	 * 
	 * @var GeoJsonObject
	 */
	protected GeoJsonObject $_feature;
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonFeature
	 */
	protected GeoJsonFeature $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('FEATURE POINT [1, 2]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('Feature', $this->_object->getType());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testGetGeometry() : void
	{
		$this->assertEquals($this->_feature, $this->_object->getGeometry());
	}
	
	public function testGetProperties() : void
	{
		$this->assertEquals(['foo' => 'bar'], $this->_object->getProperties());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_feature = new GeoJsonPoint(null, new GeoJsonPointCoordinate(1.0, 2.0));
		$this->_object = new GeoJsonFeature(null, 'id', $this->_feature, ['foo' => 'bar']);
	}
	
}
