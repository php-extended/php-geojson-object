<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonMultiLineStringCoordinateTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinate
 *
 * @internal
 *
 * @small
 */
class GeoJsonMultiLineStringCoordinateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonMultiLineStringCoordinate
	 */
	protected GeoJsonMultiLineStringCoordinate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[[[1, 2], [3, 4]], [[5, 6], [7, 8]]]', $this->_object->__toString());
	}
	
	public function testGetLineStrings() : void
	{
		$expected = new ArrayIterator([
			new GeoJsonLineStringCoordinate([
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			]),
			new GeoJsonLineStringCoordinate([
				new GeoJsonPointCoordinate(5.0, 6.0),
				new GeoJsonPointCoordinate(7.0, 8.0),
			]),
		]);
		
		$this->assertEquals($expected, $this->_object->getLineStrings());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonMultiLineStringCoordinate([
			[
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			],
			[
				new GeoJsonPointCoordinate(5.0, 6.0),
				new GeoJsonPointCoordinate(7.0, 8.0),
			],
		]);
	}
	
}
