<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonCoordinateVisitorInterface;
use PhpExtended\GeoJson\GeoJsonLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonLineStringCoordinateInterface;
use PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinateInterface;
use PhpExtended\GeoJson\GeoJsonMultiPointCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiPointCoordinateInterface;
use PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinateInterface;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinateInterface;
use PhpExtended\GeoJson\GeoJsonPolygonCoordinate;
use PhpExtended\GeoJson\GeoJsonPolygonCoordinateInterface;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonCoordinateVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonLineStringCoordinate
 * @covers \PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinate
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPointCoordinate
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinate
 * @covers \PhpExtended\GeoJson\GeoJsonPointCoordinate
 * @covers \PhpExtended\GeoJson\GeoJsonPolygonCoordinate
 *
 * @internal
 *
 * @small
 */
class GeoJsonCoordinateVisitorTest extends TestCase
{
	
	/**
	 * The visitor used to test.
	 * 
	 * @var GeoJsonCoordinateVisitorInterface
	 */
	protected GeoJsonCoordinateVisitorInterface $_visitor;
	
	public function testPoint() : void
	{
		$this->assertEquals('POINT', (new GeoJsonPointCoordinate(1.0, 2.0))->beVisitedBy($this->_visitor));
	}
	
	public function testMultiPoint() : void
	{
		$this->assertEquals('MULTI POINT', (new GeoJsonMultiPointCoordinate([]))->beVisitedBy($this->_visitor));
	}
	
	public function testLine() : void
	{
		$this->assertEquals('LINE', (new GeoJsonLineStringCoordinate([]))->beVisitedBy($this->_visitor));
	}
	
	public function testMultiLine() : void
	{
		$this->assertEquals('MULTI LINE', (new GeoJsonMultiLineStringCoordinate([]))->beVisitedBy($this->_visitor));
	}
	
	public function testPolygon() : void
	{
		$this->assertEquals('POLYGON', (new GeoJsonPolygonCoordinate([]))->beVisitedBy($this->_visitor));
	}
	
	public function testMultiPolygon() : void
	{
		$this->assertEquals('MULTI POLYGON', (new GeoJsonMultiPolygonCoordinate([]))->beVisitedBy($this->_visitor));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_visitor = new class() implements GeoJsonCoordinateVisitorInterface
		{
			
			public function __toString() : string
			{
				return 'COORDINATE VISITOR';
			}
			
			public function visitLineStringCoordinate(GeoJsonLineStringCoordinateInterface $coordinate)
			{
				return 'LINE';
			}
			
			public function visitMultiLineStringCoordinate(GeoJsonMultiLineStringCoordinateInterface $coordinate)
			{
				return 'MULTI LINE';
			}
			
			public function visitPointCoordinate(GeoJsonPointCoordinateInterface $coordinate)
			{
				return 'POINT';
			}
			
			public function visitMultiPointCoordinate(GeoJsonMultiPointCoordinateInterface $coordinate)
			{
				return 'MULTI POINT';
			}
			
			public function visitPolygonCoordinate(GeoJsonPolygonCoordinateInterface $coordinate)
			{
				return 'POLYGON';
			}
			
			public function visitMultiPolygonCoordinate(GeoJsonMultiPolygonCoordinateInterface $coordinate)
			{
				return 'MULTI POLYGON';
			}
			
		};
	}
	
}
