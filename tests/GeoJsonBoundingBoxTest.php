<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonBoundingBoxTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonBoundingBox
 *
 * @internal
 *
 * @small
 */
class GeoJsonBoundingBoxTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonBoundingBox
	 */
	protected GeoJsonBoundingBox $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[1, 2, 3, 4, 5, 6]', $this->_object->__toString());
	}
	
	public function testGetWest() : void
	{
		$this->assertEquals(1.0, $this->_object->getWest());
	}
	
	public function testGetNorth() : void
	{
		$this->assertEquals(2.0, $this->_object->getNorth());
	}
	
	public function testGetDepths() : void
	{
		$this->assertEquals(3.0, $this->_object->getDepth());
	}
	
	public function testGetEast() : void
	{
		$this->assertEquals(4.0, $this->_object->getEast());
	}
	
	public function testGetSouth() : void
	{
		$this->assertEquals(5.0, $this->_object->getSouth());
	}
	
	public function testGetHeight() : void
	{
		$this->assertEquals(6.0, $this->_object->getHeight());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
	}
	
}
