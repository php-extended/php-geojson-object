<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPolygonCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonPolygonCoordinate class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonPolygonCoordinate
 *
 * @internal
 *
 * @small
 */
class GeoJsonPolygonCoordinateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonPolygonCoordinate
	 */
	protected GeoJsonPolygonCoordinate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[[[1, 2], [3, 4]], [[5.5, 6.5], [7.5, 8.5]]]', $this->_object->__toString());
	}
	
	public function testGetExteriorLine() : void
	{
		$expected = new GeoJsonLineStringCoordinate([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
		
		$this->assertEquals($expected, $this->_object->getExteriorLine());
	}
	
	public function testGetExteriorLineFailed() : void
	{
		$this->expectException(RuntimeException::class);
		
		(new GeoJsonPolygonCoordinate([]))->getExteriorLine();
	}
	
	public function testGetInteriorLine() : void
	{
		$expected = new ArrayIterator([
			new GeoJsonLineStringCoordinate([
				new GeoJsonPointCoordinate(5.5, 6.5),
				new GeoJsonPointCoordinate(7.5, 8.5),
			]),
		]);
		
		$this->assertEquals($expected, $this->_object->getInteriorLines());
	}
	
	public function testGetLines() : void
	{
		$expected = new ArrayIterator([
			new GeoJsonLineStringCoordinate([
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			]),
			new GeoJsonLineStringCoordinate([
				new GeoJsonPointCoordinate(5.5, 6.5),
				new GeoJsonPointCoordinate(7.5, 8.5),
			]),
		]);
		
		$this->assertEquals($expected, $this->_object->getLines());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonPolygonCoordinate([
			[
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			],
			[
				new GeoJsonPointCoordinate(5.5, 6.5),
				new GeoJsonPointCoordinate(7.5, 8.5),
			],
		]);
	}
	
}
