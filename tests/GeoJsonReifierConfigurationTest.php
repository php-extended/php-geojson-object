<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonFeature;
use PhpExtended\GeoJson\GeoJsonFeatureCollection;
use PhpExtended\GeoJson\GeoJsonGeometryCollection;
use PhpExtended\GeoJson\GeoJsonLineString;
use PhpExtended\GeoJson\GeoJsonLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiLineString;
use PhpExtended\GeoJson\GeoJsonMultiLineStringCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiPoint;
use PhpExtended\GeoJson\GeoJsonMultiPointCoordinate;
use PhpExtended\GeoJson\GeoJsonMultiPolygon;
use PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinate;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPolygon;
use PhpExtended\GeoJson\GeoJsonPolygonCoordinate;
use PhpExtended\GeoJson\GeoJsonReifierConfiguration;
use PhpExtended\Reifier\Reifier;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonReifierConfiguration class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonReifierConfiguration
 *
 * @internal
 *
 * @small
 */
class GeoJsonReifierConfigurationTest extends TestCase
{
	
	/**
	 * The reifier.
	 * 
	 * @var Reifier
	 */
	protected Reifier $_reifier;
	
	public function testPoint() : void
	{
		$jspoint = \json_decode('
{
    "type": "Point",
    "coordinates": [100.0, 0.0]
}', true);
		
		/** @var GeoJsonPoint $point */
		$point = $this->_reifier->reify(GeoJsonPoint::class, $jspoint);
		
		$expected = new GeoJsonPointCoordinate(100, 0);
		
		$this->assertEquals($expected, $point->getCoordinates());
	}
	
	public function testLineString() : void
	{
		$jsline = \json_decode('
{
    "type": "LineString",
    "coordinates": [
        [100.0, 0.0],
        [101.0, 1.0]
    ]
}', true);
		
		/** @var GeoJsonLineString $line */
		$line = $this->_reifier->reify(GeoJsonLineString::class, $jsline);
		
		$expected = new GeoJsonLineStringCoordinate([
			new GeoJsonPointCoordinate(100, 0),
			new GeoJsonPointCoordinate(101, 1),
		]);
		
		$this->assertEquals($expected, $line->getCoordinates());
	}
	
	public function testPolygonNoHole() : void
	{
		$jspoly = \json_decode('
{
    "type": "Polygon",
    "coordinates": [
        [
            [100.0, 0.0],
            [101.0, 0.0],
            [101.0, 1.0],
            [100.0, 1.0],
            [100.0, 0.0]
        ]
    ]
}', true);
		
		/** @var GeoJsonLineString $poly */
		$poly = $this->_reifier->reify(GeoJsonPolygon::class, $jspoly);
		
		$expected = new GeoJsonPolygonCoordinate([
			[
				new GeoJsonPointCoordinate(100, 0),
				new GeoJsonPointCoordinate(101, 0),
				new GeoJsonPointCoordinate(101, 1),
				new GeoJsonPointCoordinate(100, 1),
				new GeoJsonPointCoordinate(100, 0),
			],
		]);
		
		$this->assertEquals($expected, $poly->getCoordinates());
	}
	
	public function testPolygonHole() : void
	{
		$jspoly = \json_decode('
{
    "type": "Polygon",
    "coordinates": [
        [
            [100.0, 0.0],
            [101.0, 0.0],
            [101.0, 1.0],
            [100.0, 1.0],
            [100.0, 0.0]
        ],
        [
            [100.8, 0.8],
            [100.8, 0.2],
            [100.2, 0.2],
            [100.2, 0.8],
            [100.8, 0.8]
        ]
    ]
}', true);
		
		/** @var GeoJsonLineString $poly */
		$poly = $this->_reifier->reify(GeoJsonPolygon::class, $jspoly);
		
		$expected = new GeoJsonPolygonCoordinate([
			[
				new GeoJsonPointCoordinate(100, 0),
				new GeoJsonPointCoordinate(101, 0),
				new GeoJsonPointCoordinate(101, 1),
				new GeoJsonPointCoordinate(100, 1),
				new GeoJsonPointCoordinate(100, 0),
			],
			[
				new GeoJsonPointCoordinate(100.8, 0.8),
				new GeoJsonPointCoordinate(100.8, 0.2),
				new GeoJsonPointCoordinate(100.2, 0.2),
				new GeoJsonPointCoordinate(100.2, 0.8),
				new GeoJsonPointCoordinate(100.8, 0.8),
			],
		]);
		
		$this->assertEquals($expected, $poly->getCoordinates());
	}
	
	public function testMultiPoint() : void
	{
		$jsmpt = \json_decode('
{
    "type": "MultiPoint",
    "coordinates": [
        [100.0, 0.0],
        [101.0, 1.0]
    ]
}', true);
		
		/** @var GeoJsonMultiPoint $mpoint */
		$mpoint = $this->_reifier->reify(GeoJsonMultiPoint::class, $jsmpt);
		
		$expected = new GeoJsonMultiPointCoordinate([
			new GeoJsonPointCoordinate(100, 0),
			new GeoJsonPointCoordinate(101, 1),
		]);
		
		$this->assertEquals($expected, $mpoint->getCoordinates());
	}
	
	public function testMultiLineString() : void
	{
		$jsmline = \json_decode('
{
    "type": "MultiLineString",
    "coordinates": [
        [
            [100.0, 0.0],
            [101.0, 1.0]
        ],
        [
            [102.0, 2.0],
            [103.0, 3.0]
        ]
    ]
}', true);
		
		/** @var GeoJsonMultiPoint $mline */
		$mline = $this->_reifier->reify(GeoJsonMultiLineString::class, $jsmline);
		
		$expected = new GeoJsonMultiLineStringCoordinate([
			[
				new GeoJsonPointCoordinate(100, 0),
				new GeoJsonPointCoordinate(101, 1),
			],
			[
				new GeoJsonPointCoordinate(102, 2),
				new GeoJsonPointCoordinate(103, 3),
			],
		]);
		
		$this->assertEquals($expected, $mline->getCoordinates());
	}
	
	public function testMultiPolygon() : void
	{
		$jsmpoly = \json_decode('
{
    "type": "MultiPolygon",
    "coordinates": [
        [
            [
                [102.0, 2.0],
                [103.0, 2.0],
                [103.0, 3.0],
                [102.0, 3.0],
                [102.0, 2.0]
            ]
        ],
        [
            [
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
            ],
            [
                [100.2, 0.2],
                [100.2, 0.8],
                [100.8, 0.8],
                [100.8, 0.2],
                [100.2, 0.2]
            ]
        ]
    ]
}', true);
		
		/** @var GeoJsonMultiPolygon $mpoly */
		$mpoly = $this->_reifier->reify(GeoJsonMultiPolygon::class, $jsmpoly);
		
		$expected = new GeoJsonMultiPolygonCoordinate([
			[
				[
					new GeoJsonPointCoordinate(102, 2),
					new GeoJsonPointCoordinate(103, 2),
					new GeoJsonPointCoordinate(103, 3),
					new GeoJsonPointCoordinate(102, 3),
					new GeoJsonPointCoordinate(102, 2),
				],
			],
			[
				[
					new GeoJsonPointCoordinate(100, 0),
					new GeoJsonPointCoordinate(101, 0),
					new GeoJsonPointCoordinate(101, 1),
					new GeoJsonPointCoordinate(100, 1),
					new GeoJsonPointCoordinate(100, 0),
				],
				[
					new GeoJsonPointCoordinate(100.2, 0.2),
					new GeoJsonPointCoordinate(100.2, 0.8),
					new GeoJsonPointCoordinate(100.8, 0.8),
					new GeoJsonPointCoordinate(100.8, 0.2),
					new GeoJsonPointCoordinate(100.2, 0.2),
				],
			],
		]);
		
		$this->assertEquals($expected, $mpoly->getCoordinates());
	}
	
	public function testGeometryCollection() : void
	{
		$jsgeoc = \json_decode('
{
    "type": "GeometryCollection",
    "geometries": [{
        "type": "Point",
        "coordinates": [100.0, 0.0]
    }, {
        "type": "LineString",
        "coordinates": [
            [101.0, 0.0],
            [102.0, 1.0]
        ]
    }]
}', true);
		
		/** @var GeoJsonGeometryCollection $geomc */
		$geomc = $this->_reifier->reify(GeoJsonGeometryCollection::class, $jsgeoc);
		
		foreach($geomc->getGeometries() as $k => $geometry)
		{
			if(0 === $k)
			{
				$this->assertInstanceOf(GeoJsonPoint::class, $geometry);
			}
			else
			{ 
				$this->assertInstanceOf(GeoJsonLineString::class, $geometry);
			}
		}
	}
	
	public function testFeature() : void
	{
		$jsfeat = \json_decode('
{
    "type": "Feature",
    "bbox": [-10.0, -10.0, 10.0, 10.0],
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [
                [-10.0, -10.0],
                [10.0, -10.0],
                [10.0, 10.0],
                [-10.0, -10.0]
            ]
        ]
    }
}', true);
		
		/** @var GeoJsonFeature $feature */
		$feature = $this->_reifier->reify(GeoJsonFeature::class, $jsfeat);
		
		$this->assertInstanceOf(GeoJsonPolygon::class, $feature->getGeometry());
	}
	
	public function testRealFeature() : void
	{
		$jsfeat = \json_decode('
{"type":"Feature","properties":{"nom":"Abrest","code":"03001","codeDepartement":"03",
"codeRegion":"84","codesPostaux":["03200"],"population":2923},"geometry":{"type":"Polygon","coordinates":
[[[3.415027,46.098143],[3.415631,46.09822],[3.415761,46.09807],[3.415952,46.097932],[3.416062,46.097937],[3.416143,46.098043],[3.41631,46.098104],[3.416429,46.098055],
[3.416686,46.098165],[3.416769,46.098077],[3.417088,46.098099],[3.417276,46.098291],[3.417595,46.098457],[3.417797,46.098661],[3.417894,46.098623],[3.418164,46.098751],
[3.418425,46.098842],[3.418439,46.098908],[3.41863,46.099033],[3.418696,46.099008],[3.41914,46.099156],[3.419607,46.09944],[3.420018,46.099393],[3.420068,46.099411],
[3.420038,46.099636],[3.420191,46.099594],[3.420269,46.099661],[3.42019,46.099794],[3.420233,46.099873],[3.420133,46.099948],[3.420228,46.100092],[3.420245,46.100212],
[3.419971,46.10044],[3.419963,46.100564],[3.420068,46.10069],[3.420089,46.100852],[3.421002,46.101047],[3.421449,46.101112],[3.421793,46.101263],[3.422139,46.10158],
[3.422461,46.101815],[3.423038,46.102145],[3.423205,46.102436],[3.42335,46.102587],[3.42347,46.102644],[3.424341,46.102798],[3.424637,46.10291],[3.424714,46.103046],
[3.424419,46.103904],[3.424288,46.104216],[3.424121,46.104497],[3.424101,46.104652],[3.423745,46.104961],[3.423173,46.105554],[3.422716,46.106116],[3.422514,46.106517],
[3.422449,46.106702],[3.42238,46.107428],[3.422444,46.107765],[3.422718,46.108284],[3.422823,46.108525],[3.423073,46.108798],[3.42393,46.109568],[3.424463,46.110205],
[3.424639,46.110351],[3.425214,46.110735],[3.435736,46.112934],[3.437436,46.111451],[3.437491,46.111433],[3.43785,46.111879],[3.438172,46.112139],[3.438305,46.112336],
[3.438343,46.11255],[3.438227,46.112835],[3.43783,46.113348],[3.437883,46.113358],[3.439043,46.113394],[3.439894,46.112708],[3.440865,46.112588],[3.440936,46.112496],
[3.441447,46.112515],[3.442579,46.112232],[3.442991,46.111997],[3.443905,46.110982],[3.444133,46.110868],[3.445296,46.11056],[3.445394,46.110552],[3.445565,46.110529],
[3.446472,46.110232],[3.446736,46.110119],[3.447266,46.109617],[3.447517,46.109403],[3.448091,46.108991],[3.449748,46.108137],[3.450254,46.107963],[3.449598,46.107016],
[3.449469,46.106769],[3.449338,46.106202],[3.449261,46.106035],[3.449067,46.10598],[3.449152,46.105723],[3.449971,46.105843],[3.450066,46.105715],[3.450928,46.105792],
[3.45185,46.105772],[3.452051,46.1057],[3.452504,46.105172],[3.452599,46.105028],[3.452582,46.104536],[3.4526,46.104468],[3.452829,46.104093],[3.453083,46.103857],
[3.453805,46.103505],[3.454021,46.103423],[3.45479,46.102909],[3.456632,46.101589],[3.457455,46.10089],[3.458219,46.100026],[3.458327,46.099994],[3.460701,46.101047],
[3.461038,46.101311],[3.46109,46.101326],[3.467574,46.099765],[3.467647,46.099775],[3.467082,46.101069],[3.467031,46.101295],[3.466939,46.101985],[3.466763,46.102224],
[3.466645,46.102297],[3.466748,46.102378],[3.46882,46.101628],[3.469864,46.101282],[3.471126,46.100991],[3.471575,46.10094],[3.471971,46.100842],[3.474597,46.099933],
[3.474805,46.099846],[3.475737,46.099381],[3.477488,46.098261],[3.477918,46.09794],[3.479082,46.096818],[3.479115,46.096749],[3.479148,46.09552],[3.479223,46.095292],
[3.479372,46.095003],[3.479586,46.094652],[3.47974,46.094452],[3.480069,46.09415],[3.480839,46.093699],[3.48109,46.093495],[3.481531,46.093051],[3.481723,46.092911],
[3.482551,46.092441],[3.482781,46.092265],[3.482975,46.091965],[3.483256,46.091303],[3.483563,46.090882],[3.483793,46.090506],[3.483906,46.090236],[3.483938,46.089944],
[3.483921,46.0896],[3.484039,46.089348],[3.484314,46.088947],[3.48456,46.08866],[3.485247,46.087927],[3.485819,46.087349],[3.485907,46.087223],[3.486035,46.086752],
[3.486077,46.086757],[3.486512,46.087188],[3.486694,46.087418],[3.486923,46.087615],[3.487113,46.087428],[3.487832,46.086801],[3.487606,46.08657],[3.487345,46.086473],
[3.486872,46.086222],[3.486799,46.086148],[3.486614,46.085741],[3.486468,46.085645],[3.486416,46.085547],[3.486485,46.085412],[3.486374,46.08519],[3.486316,46.084916],
[3.486169,46.084687],[3.486053,46.084398],[3.485832,46.084114],[3.485709,46.083857],[3.485487,46.083682],[3.485311,46.083617],[3.484826,46.083595],[3.484363,46.083526],
[3.484066,46.083403],[3.483873,46.083187],[3.483846,46.083087],[3.483493,46.08297],[3.483333,46.082793],[3.482913,46.082539],[3.48246,46.082482],[3.48218,46.082177],
[3.481594,46.082243],[3.48106,46.082243],[3.480809,46.082272],[3.480188,46.082224],[3.479983,46.08209],[3.479653,46.082139],[3.479501,46.082271],[3.479283,46.082258],
[3.479147,46.082308],[3.478832,46.08236],[3.478557,46.082539],[3.478428,46.082565],[3.478066,46.082845],[3.477874,46.082772],[3.477709,46.082782],[3.47714,46.082889],
[3.476866,46.082797],[3.476659,46.082679],[3.476457,46.082652],[3.476201,46.082713],[3.475934,46.082727],[3.475834,46.082617],[3.475498,46.082523],[3.47522,46.082535],
[3.475085,46.08265],[3.474913,46.082682],[3.474648,46.082586],[3.474462,46.082657],[3.474268,46.082634],[3.474121,46.082557],[3.473851,46.082566],[3.473685,46.082633],
[3.473452,46.082539],[3.473345,46.082621],[3.473209,46.082639],[3.472955,46.082532],[3.472549,46.082454],[3.472207,46.082246],[3.471984,46.08197],[3.471859,46.081951],
[3.471832,46.082177],[3.471681,46.082074],[3.47144,46.082018],[3.47129,46.081862],[3.470909,46.081815],[3.470629,46.081741],[3.470433,46.081855],[3.470341,46.081865],
[3.470165,46.081783],[3.470072,46.081497],[3.469889,46.081341],[3.469427,46.081276],[3.469305,46.081323],[3.468863,46.081112],[3.468617,46.081135],[3.46822,46.081125],
[3.467563,46.080993],[3.467248,46.080966],[3.467051,46.080979],[3.466733,46.080954],[3.466356,46.080796],[3.466197,46.080866],[3.465654,46.080797],[3.465481,46.080847],
[3.465106,46.080755],[3.465022,46.080659],[3.464747,46.080626],[3.464513,46.080536],[3.464289,46.080537],[3.46408,46.080446],[3.46391,46.080442],[3.463705,46.080327],
[3.463277,46.080174],[3.462024,46.079371],[3.461767,46.079263],[3.461254,46.079176],[3.460166,46.080105],[3.460057,46.080459],[3.45981,46.081097],[3.459711,46.081303],
[3.459167,46.082213],[3.458476,46.083306],[3.457676,46.084378],[3.457434,46.084714],[3.457024,46.08512],[3.45616,46.085875],[3.455699,46.086248],[3.455371,46.086483],
[3.454843,46.086714],[3.45396,46.087014],[3.453529,46.087191],[3.452702,46.087591],[3.452539,46.087688],[3.452222,46.087938],[3.451309,46.088443],[3.450977,46.088709],
[3.450048,46.089925],[3.44937,46.090643],[3.448622,46.091315],[3.447268,46.092299],[3.447068,46.092522],[3.44659,46.092938],[3.446204,46.093424],[3.445836,46.093717],
[3.445224,46.094313],[3.444725,46.094707],[3.444447,46.094877],[3.443941,46.095145],[3.443419,46.095396],[3.442841,46.095647],[3.442338,46.095912],[3.440652,46.096918],
[3.43942,46.09494],[3.438669,46.093777],[3.438227,46.092981],[3.43815,46.092784],[3.438114,46.092519],[3.437954,46.0925],[3.43738,46.09137],[3.43649,46.089751],
[3.4362,46.089381],[3.435038,46.087426],[3.434953,46.08738],[3.434416,46.087228],[3.433798,46.087137],[3.433059,46.087063],[3.432788,46.087062],[3.432254,46.087105],
[3.431555,46.087224],[3.430707,46.087413],[3.430355,46.087532],[3.42979,46.087821],[3.429497,46.088003],[3.429282,46.088175],[3.428969,46.088518],[3.428723,46.08901],
[3.428291,46.089673],[3.428116,46.089851],[3.427474,46.09016],[3.427446,46.090033],[3.427259,46.089675],[3.427027,46.089354],[3.427014,46.089172],[3.426921,46.089026],
[3.426691,46.088919],[3.426524,46.088771],[3.426433,46.088565],[3.42624,46.088575],[3.426028,46.088638],[3.425699,46.088808],[3.425382,46.088845],[3.425121,46.088908],
[3.425051,46.088958],[3.424816,46.089307],[3.424596,46.08943],[3.424511,46.089593],[3.42444,46.089887],[3.424316,46.090032],[3.424048,46.090159],[3.423671,46.090268],
[3.423526,46.090286],[3.423179,46.090208],[3.422788,46.090153],[3.422483,46.090085],[3.422355,46.090105],[3.422107,46.090248],[3.421709,46.090368],[3.421647,46.090457],
[3.421375,46.090549],[3.421333,46.090624],[3.421034,46.090621],[3.420477,46.090469],[3.42032,46.090404],[3.420037,46.090161],[3.419415,46.089873],[3.419197,46.089789],
[3.418528,46.090086],[3.418526,46.0902],[3.417771,46.09045],[3.416738,46.09084],[3.416771,46.090969],[3.416661,46.091734],[3.416525,46.091796],[3.416473,46.092081],
[3.416425,46.092112],[3.416351,46.092463],[3.416358,46.092607],[3.416104,46.093621],[3.416142,46.094206],[3.416066,46.094462],[3.416134,46.094697],[3.416159,46.095196],
[3.416131,46.095342],[3.415964,46.095652],[3.415926,46.095855],[3.415861,46.095929],[3.415668,46.095922],[3.415372,46.096168],[3.415283,46.096294],[3.415382,46.096478],
[3.415393,46.097084],[3.415323,46.097303],[3.415422,46.097464],[3.415412,46.097602],[3.415048,46.098092],[3.415027,46.098143]]]}}', true);
		
		/** @var GeoJsonFeature $feature */
		$feature = $this->_reifier->reify(GeoJsonFeature::class, $jsfeat);
		
		$this->assertInstanceOf(GeoJsonPolygon::class, $feature->getGeometry());
	}
	
	public function testFeatureCollection() : void
	{
		$jsftcl = \json_decode('
{
    "type": "FeatureCollection",
    "bbox": [100.0, 0.0, 12, 105.0, 1.0, 14],
    "features": [
        {
            "type": "Feature",
            "bbox": [-10.0, -10.0, 10.0, 10.0],
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [-10.0, -10.0],
                        [10.0, -10.0],
                        [10.0, 10.0],
                        [-10.0, -10.0]
                    ]
                ]
            }
        }
    ]
}', true);
		
		/** @var GeoJsonFeatureCollection $ftcoll */
		$ftcoll = $this->_reifier->reify(GeoJsonFeatureCollection::class, $jsftcl);
		
		foreach($ftcoll->getFeatures() as $feature)
		{
			$this->assertInstanceOf(GeoJsonFeature::class, $feature);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_reifier = new Reifier();
		$this->_reifier->setConfiguration(new GeoJsonReifierConfiguration());
	}
	
}
