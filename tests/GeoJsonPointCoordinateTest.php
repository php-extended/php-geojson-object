<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @small
 * @covers \PhpExtended\GeoJson\GeoJsonPointCoordinate
 */
class GeoJsonPointCoordinateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonPointCoordinate
	 */
	protected GeoJsonPointCoordinate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[1.5, 2.5, 3.5]', $this->_object->__toString());
	}
	
	public function testGetLatitude() : void
	{
		$this->assertEquals(2.5, $this->_object->getLatitude());
	}
	
	public function testGetLongitude() : void
	{
		$this->assertEquals(1.5, $this->_object->getLongitude());
	}
	
	public function testGetAltitude() : void
	{
		$this->assertEquals(3.5, $this->_object->getAltitude());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonPointCoordinate(1.5, 2.5, 3.5);
	}
	
}
