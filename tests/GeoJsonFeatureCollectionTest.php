<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonFeature;
use PhpExtended\GeoJson\GeoJsonFeatureCollection;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonFeatureCollectionTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonFeatureCollection
 *
 * @internal
 *
 * @small
 */
class GeoJsonFeatureCollectionTest extends TestCase
{
	
	/**
	 * The feature.
	 * 
	 * @var GeoJsonFeature
	 */
	protected GeoJsonFeature $_feature;
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonFeatureCollection
	 */
	protected GeoJsonFeatureCollection $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('FEATURE COLLECTION [FEATURE POINT [1, 2]]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('FeatureCollection', $this->_object->getType());
	}
	
	public function testGetFeature() : void
	{
		$this->assertEquals(new ArrayIterator([$this->_feature]), $this->_object->getFeatures());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_feature = new GeoJsonFeature(
			null,
			'id',
			new GeoJsonPoint(null, new GeoJsonPointCoordinate(1.0, 2.0)),
			['foo' => 'bar'],
		);
		$this->_object = new GeoJsonFeatureCollection(null, [$this->_feature]);
	}
	
}
