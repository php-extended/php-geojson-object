<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonGeometryCollection;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonGeometryCollectionTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonGeometryCollection
 *
 * @internal
 *
 * @small
 */
class GeoJsonGeometryCollectionTest extends TestCase
{
	
	/**
	 * @var GeoJsonPoint
	 */
	protected GeoJsonPoint $_geometry;
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonGeometryCollection
	 */
	protected GeoJsonGeometryCollection $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('COLLECTION [POINT [1, 2]]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('GeometryCollection', $this->_object->getType());
	}
	
	public function testGetGeometries() : void
	{
		$this->assertEquals(new ArrayIterator([$this->_geometry]), $this->_object->getGeometries());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_geometry = new GeoJsonPoint(null, new GeoJsonPointCoordinate(1.0, 2.0));
		$this->_object = new GeoJsonGeometryCollection(
			new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
			[$this->_geometry],
		);
	}
	
}
