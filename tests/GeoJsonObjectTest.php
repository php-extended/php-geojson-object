<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonObject;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonObject class file.
 * 
 * @author Anastaszor
 *
 * @internal
 *
 * @small
 * @covers \PhpExtended\GeoJson\GeoJsonObject
 */
class GeoJsonObjectTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonObject
	 */
	protected GeoJsonObject $_object;
	
	public function testGetBbox() : void
	{
		$expected = new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
		$this->assertEquals($expected, $this->_object->getBoundingBox());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$bbox = new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
		$this->_object = $this->getMockForAbstractClass(GeoJsonObject::class, [$bbox]);
	}
	
}
