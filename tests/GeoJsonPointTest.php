<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonPoint class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonPoint
 *
 * @internal
 *
 * @small
 */
class GeoJsonPointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonPoint
	 */
	protected GeoJsonPoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('POINT [1, 2]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('Point', $this->_object->getType());
	}
	
	public function testGetCoordinates() : void
	{
		$expected = new GeoJsonPointCoordinate(1.0, 2.0);
		$this->assertEquals($expected, $this->_object->getCoordinates());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonPoint(new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0), new GeoJsonPointCoordinate(1.0, 2.0));
	}
	
}
