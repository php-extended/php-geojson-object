<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonGeometryCollection;
use PhpExtended\GeoJson\GeoJsonGeometryCollectionInterface;
use PhpExtended\GeoJson\GeoJsonGeometryVisitorInterface;
use PhpExtended\GeoJson\GeoJsonLineString;
use PhpExtended\GeoJson\GeoJsonLineStringInterface;
use PhpExtended\GeoJson\GeoJsonMultiLineString;
use PhpExtended\GeoJson\GeoJsonMultiLineStringInterface;
use PhpExtended\GeoJson\GeoJsonMultiPoint;
use PhpExtended\GeoJson\GeoJsonMultiPointInterface;
use PhpExtended\GeoJson\GeoJsonMultiPolygon;
use PhpExtended\GeoJson\GeoJsonMultiPolygonInterface;
use PhpExtended\GeoJson\GeoJsonPoint;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPointInterface;
use PhpExtended\GeoJson\GeoJsonPolygon;
use PhpExtended\GeoJson\GeoJsonPolygonInterface;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonGeometryVisitorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonGeometryCollection
 * @covers \PhpExtended\GeoJson\GeoJsonLineString
 * @covers \PhpExtended\GeoJson\GeoJsonMultiLineString
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPoint
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPolygon
 * @covers \PhpExtended\GeoJson\GeoJsonPoint
 * @covers \PhpExtended\GeoJson\GeoJsonPolygon
 *
 * @internal
 *
 * @small
 */
class GeoJsonGeometryVisitorTest extends TestCase
{
	
	/**
	 * The visitor to help test.
	 * 
	 * @var GeoJsonGeometryVisitorInterface
	 */
	protected GeoJsonGeometryVisitorInterface $_visitor;
	
	public function testPoint() : void
	{
		$this->assertEquals('POINT', (new GeoJsonPoint(null, new GeoJsonPointCoordinate(1, 2)))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testMultiPoint() : void
	{
		$this->assertEquals('MULTI POINT', (new GeoJsonMultiPoint(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testLine() : void
	{
		$this->assertEquals('LINE', (new GeoJsonLineString(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testMultiLine() : void
	{
		$this->assertEquals('MULTI LINE', (new GeoJsonMultiLineString(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testPolygon() : void
	{
		$this->assertEquals('POLYGON', (new GeoJsonPolygon(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testMultiPolygon() : void
	{
		$this->assertEquals('MULTI POLYGON', (new GeoJsonMultiPolygon(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	public function testGeometryCollection() : void
	{
		$this->assertEquals('GEOMETRY COLLECTION', (new GeoJsonGeometryCollection(null, []))->beVisitedByGeometry($this->_visitor));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_visitor = new class() implements GeoJsonGeometryVisitorInterface
		{
			
			public function __toString() : string
			{
				return 'GEOMETRY VISITOR';
			}
			
			public function visitGeometryCollection(GeoJsonGeometryCollectionInterface $collection)
			{
				return 'GEOMETRY COLLECTION';
			}
			
			public function visitLineString(GeoJsonLineStringInterface $lineString)
			{
				return 'LINE';
			}
			
			public function visitMultiLineString(GeoJsonMultiLineStringInterface $multiLineString)
			{
				return 'MULTI LINE';
			}
			
			public function visitPoint(GeoJsonPointInterface $point)
			{
				return 'POINT';
			}
			
			public function visitMultiPoint(GeoJsonMultiPointInterface $multiPoint)
			{
				return 'MULTI POINT';
			}
			
			public function visitPolygon(GeoJsonPolygonInterface $polygon)
			{
				return 'POLYGON';
			}
			
			public function visitMultiPolygon(GeoJsonMultiPolygonInterface $multiPolygon)
			{
				return 'MULTI POLYGON';
			}
			
		};
	}
	
}
