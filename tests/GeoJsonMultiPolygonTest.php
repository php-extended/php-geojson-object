<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonMultiPolygon;
use PhpExtended\GeoJson\GeoJsonMultiPolygonCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonMultiPolygonTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPolygon
 *
 * @internal
 *
 * @small
 */
class GeoJsonMultiPolygonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonMultiPolygon
	 */
	protected GeoJsonMultiPolygon $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('MULTI POLYGON [[[[1, 2], [3, 4]], [[5.5, 6.5], [7.5, 8.5]]], [[[11, 12], [13, 14]], [[15.5, 16.5], [17.5, 18.5]]]]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('MultiPolygon', $this->_object->getType());
	}
	
	public function testGetCoordinates() : void
	{
		$expected = new GeoJsonMultiPolygonCoordinate([
			[
				[
					new GeoJsonPointCoordinate(1.0, 2.0),
					new GeoJsonPointCoordinate(3.0, 4.0),
				],
				[
					new GeoJsonPointCoordinate(5.5, 6.5),
					new GeoJsonPointCoordinate(7.5, 8.5),
				],
			],
			[
				[
					new GeoJsonPointCoordinate(11.0, 12.0),
					new GeoJsonPointCoordinate(13.0, 14.0),
				],
				[
					new GeoJsonPointCoordinate(15.5, 16.5),
					new GeoJsonPointCoordinate(17.5, 18.5),
				],
			],
		]);
		
		$this->assertEquals($expected, $this->_object->getCoordinates());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonMultiPolygon(
			new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
			[
				[
					[
						new GeoJsonPointCoordinate(1.0, 2.0),
						new GeoJsonPointCoordinate(3.0, 4.0),
					],
					[
						new GeoJsonPointCoordinate(5.5, 6.5),
						new GeoJsonPointCoordinate(7.5, 8.5),
					],
				],
				[
					[
						new GeoJsonPointCoordinate(11.0, 12.0),
						new GeoJsonPointCoordinate(13.0, 14.0),
					],
					[
						new GeoJsonPointCoordinate(15.5, 16.5),
						new GeoJsonPointCoordinate(17.5, 18.5),
					],
				],
			],
		);
	}
	
}
