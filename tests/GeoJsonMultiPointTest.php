<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonBoundingBox;
use PhpExtended\GeoJson\GeoJsonMultiPoint;
use PhpExtended\GeoJson\GeoJsonMultiPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonMultiPointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPoint
 *
 * @internal
 *
 * @small
 */
class GeoJsonMultiPointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonMultiPoint
	 */
	protected GeoJsonMultiPoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('MULTI POINT [[1, 2], [3, 4]]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('MultiPoint', $this->_object->getType());
	}
	
	public function testGetCoordinate() : void
	{
		$expected = new GeoJsonMultiPointCoordinate([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
		
		$this->assertEquals($expected, $this->_object->getCoordinates());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonMultiPoint(new GeoJsonBoundingBox(1.0, 2.0, 3.0, 4.0, 5.0, 6.0), [
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
	}
	
}
