<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonMultiPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonMultiPointCoordinateTest test case.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonMultiPointCoordinate
 *
 * @internal
 *
 * @small
 */
class GeoJsonMultiPointCoordinateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonMultiPointCoordinate
	 */
	protected GeoJsonMultiPointCoordinate $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[[1, 2], [3, 4]]', $this->_object->__toString());
	}
	
	public function testGetPoints() : void
	{
		$expected = new ArrayIterator([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
		
		$this->assertEquals($expected, $this->_object->getPoints());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonMultiPointCoordinate([
			new GeoJsonPointCoordinate(1.0, 2.0),
			new GeoJsonPointCoordinate(3.0, 4.0),
		]);
	}
	
}
