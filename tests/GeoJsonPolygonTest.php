<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\GeoJson\GeoJsonPointCoordinate;
use PhpExtended\GeoJson\GeoJsonPolygon;
use PhpExtended\GeoJson\GeoJsonPolygonCoordinate;
use PHPUnit\Framework\TestCase;

/**
 * GeoJsonPolygonTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\GeoJson\GeoJsonPolygon
 *
 * @internal
 *
 * @small
 */
class GeoJsonPolygonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var GeoJsonPolygon
	 */
	protected GeoJsonPolygon $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('POLYGON [[[1, 2], [3, 4]], [[5.5, 6.5], [7.5, 8.5]]]', $this->_object->__toString());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('Polygon', $this->_object->getType());
	}
	
	public function testGetCoordinates() : void
	{
		$expected = new GeoJsonPolygonCoordinate([
			[
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			],
			[
				new GeoJsonPointCoordinate(5.5, 6.5),
				new GeoJsonPointCoordinate(7.5, 8.5),
			],
		]);
		
		$this->assertEquals($expected, $this->_object->getCoordinates());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new GeoJsonPolygon(null, [
			[
				new GeoJsonPointCoordinate(1.0, 2.0),
				new GeoJsonPointCoordinate(3.0, 4.0),
			],
			[
				new GeoJsonPointCoordinate(5.5, 6.5),
				new GeoJsonPointCoordinate(7.5, 8.5),
			],
		]);
	}
	
}
