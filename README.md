# php-extended/php-geojson-object
An implementation of the php-geojson-interface library

![coverage](https://gitlab.com/php-extended/php-geojson-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-geojson-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-geojson-object ^8`


## Basic Usage

This library is made to be used in other libraries that transforms json data
from the php-json-object library.

The most common way to use it is to place in an object's constructor :

```php

// $value is the current mixed[] thing
// $silent is a boolean

	case 'geojson':
		$factory = new GeoJsonGeometryFactory();
		$this->_geojson = $factory->buildObject($this->asArray($value, $silent), $silent);
		break;

```

Please see how a `PhpExtended\GeoJson\GeoJsonPoint` is constructed 
for a better example and a more complete analogy.


## License

MIT (See [license file](LICENSE)).
